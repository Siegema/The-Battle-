﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTest : MonoBehaviour
{

    public UnityEvent testEvnt;

    public bool testing;

    // Use this for initialization
    void Start()
    {
        testing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (testing)
        {
            testEvnt.Invoke();
            testing = false;
        }

    }
}
