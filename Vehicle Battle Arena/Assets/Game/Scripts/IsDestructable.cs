﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class IsDestructable : MonoBehaviour
{

    private PhotonView PV;

    public void OnEnable()
    {
        PV = GetComponent<PhotonView>();
    }

    public void DestroyInstance()
    {
        DestroyOnNetwork.instance.Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<IsDestructable>())
        {
            DestroyInstance();
        }
    }
}
