﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using System.IO;

public class Room : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    public string bundleName;

    public static Room instance;

    public int currentScene;

    public int testScene;


    //Player Info
    Player[] photonPlayers;
    public int playersInRoom;
    public int number;//Player number in Room
    public int playerInGame;

    public Vector3[] spawnPoints;

    public int colorIndex;

    public bool isBot = false;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != this)
            { 
                Destroy(instance);
                instance = this;
            }
        }

        DontDestroyOnLoad(this.gameObject);
    } 

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Debug.Log("Joined Room:" + PhotonNetwork.CurrentRoom.Name);

        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom = photonPlayers.Length;
        number = playersInRoom;
        PhotonNetwork.NickName = number.ToString();
        loadLobby();
    }

    public override void OnLeftRoom()
    { 
        base.OnLeftRoom();
        playersInRoom--;
        number--;
        ExitGame();
    } 

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer); 

        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom++;
    }

    public void load(string levelName)
    {
        bundleName = levelName;
        StartGame();
    }


    private void ExitGame()
    {
        PhotonNetwork.LoadLevel("GLOBAL");
    }

    private void loadLobby()
    {
        PhotonNetwork.LoadLevel("Character Selection");
    }

    private void StartGame()
    {
        PhotonNetwork.LoadLevel(bundleName);
        // ------------- TESTING -------------
//        string path = Path.Combine(Application.streamingAssetsPath, bundleName);
//        var myLoadedAssetBundle = AssetBundle.LoadFromFile(path);
//        if (myLoadedAssetBundle == null)
//        {
//            Debug.Log("Failed to load AssetBundle");
//            return;
//        }
//
//        if (myLoadedAssetBundle.isStreamedSceneAssetBundle)
//        {
//            string[] scenePaths = myLoadedAssetBundle.GetAllScenePaths();
//            string sceneName = Path.GetFileNameWithoutExtension(scenePaths[0]);
//            //SceneManager.LoadScene(sceneName);
//            PhotonNetwork.LoadLevel(sceneName);
//        }
        // ------------- TESTING -------------

        //     if (PhotonNetwork.IsMasterClient)
        //     {
        //         PhotonNetwork.CurrentRoom.IsOpen = false;


        //         //PhotonNetwork.LoadLevel("TestLevel");
        //         //MultiplayerSetting.instance.gameScene
        //     }
    }

    private void TestLevel()
    {
        PhotonNetwork.LoadLevel(MultiplayerSetting.instance.gameScene);
    }
}
