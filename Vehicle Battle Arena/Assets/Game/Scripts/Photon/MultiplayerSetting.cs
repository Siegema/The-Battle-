﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerSetting : MonoBehaviour {

    public static MultiplayerSetting instance;

    public bool delayStart;
    public int maxPlayers;

    public int menuScene;
    public int gameScene;
    public int selectionScene;

    void Awake()
    { 
        if(instance == null)
        {
            instance = this;
        }
        else
        { 
            if(instance != this)
            { 
                Destroy(instance);
                instance = this;
            }
        }

      //  DontDestroyOnLoad(this.gameObject);
    } 
}
