﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class EventTarget : MonoBehaviour
{
    [System.Serializable]
    public class KickEvent : UnityEvent<int> {}
    public KickEvent kEvnt;

	public GameObject target;
    public int playerNumber;

    byte evntCode = 0;
    List<object> content = new List<object>();
    RaiseEventOptions eventTarget = new RaiseEventOptions { Receivers = ReceiverGroup.All };
    SendOptions sendOptions = new SendOptions { Reliability = true };

    PhotonView PV;
    Button btn;

    private void Awake()
    {
        btn = GetComponent<Button>();
    }

    void OnEnable()
    {
        btn.onClick.AddListener(Kick);
    }

    void OnDisable()
    {
        btn.onClick.RemoveListener(Kick);
    }

    // Use this for initialization
    void Start()
    {
        PV = target.GetComponent<PhotonView>();
    }

    void Kick()
    {
        kEvnt.Invoke(playerNumber);
        content.Insert(0, playerNumber);
        PhotonNetwork.RaiseEvent(evntCode, content, eventTarget, sendOptions);
    }
}
