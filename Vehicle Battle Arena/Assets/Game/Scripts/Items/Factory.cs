﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Factory : MonoBehaviour, IPickup<Transform>
{ 
	public GameObject[] spawnObjects;
	public float launchTime; 

	[Tooltip("How high the factory will shoot the object")]
	public float height;
	public float spread;

    [SerializeField]
    private float lifeSpan;

    private float timer;

    [SerializeField]
    private float spawnDistance = 2.0f;

    private Vector3[] directions = { Vector3.right
                                    ,Vector3.left
                                    ,Vector3.up
                                    ,Vector3.down};

    // Use this for initialization
    void Start()
    {
        timer = launchTime;
    }

    // Update is called once per frame
    void Update()
    { 
		TimerUpdt();
    }

	private void TimerUpdt()
	{
        timer -= Time.deltaTime;
		if(timer <= 0.0f)
		{ 
			Spawn();
		}

        lifeSpan -= Time.deltaTime;
        if(lifeSpan <= 0.0f)
        {
            Despawn();
        }
	}

	private void Spawn()
	{
        int index = Random.Range(0, spawnObjects.Length);

        GameObject gObj = PhotonNetwork.Instantiate("Prefabs/Items/"+spawnObjects[index].name, transform.position, transform.rotation);

        Vector3 dir = directions[Random.Range(0, directions.Length)]; 
        float h = height;  
        float dist = dir.magnitude;  
        dir.y = h; 
        dist += spread;  
        float vel = Mathf.Sqrt(dist * Physics.gravity.magnitude);

        gObj.GetComponent<Rigidbody>().velocity =  vel * dir.normalized;  

        timer = launchTime;
	}

    private void Despawn()
    {
        DestroyOnNetwork.instance.Destroy(this.gameObject);
    }

    public void UsePickup(Transform transform)
    {
        Vector3 spawnPos = transform.position - transform.forward * spawnDistance;

        PhotonNetwork.Instantiate("Prefabs/Items/"+this.gameObject.name, spawnPos, this.transform.rotation);
    }
}
