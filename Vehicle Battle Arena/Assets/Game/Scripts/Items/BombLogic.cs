﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombLogic : MonoBehaviour
{
    public float lifeSpan = 7.0f;

    private float timer = 0.0f;
    private GameObject target;

    private FieldOfView fov;
    private SeekSteeringBehaviour ssb;

    [SerializeField]
    private GameObject explosion;

    private void Start()
    {
        timer = 0.0f;
        fov = GetComponent<FieldOfView>();
        ssb = GetComponent<SeekSteeringBehaviour>();
    } 

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer >= lifeSpan)
        {
            Destroy(this.gameObject);
        }

        if (fov != null)
        {
            if (fov.visibleTargets.Count > 0)
            {
                target = fov.visibleTargets[0].gameObject;
            }

            if (target)
            {
                ssb.SetTarget(target);
            }
        }
    }

    private void OnDestroy()
    { 
        Instantiate(explosion, transform.position, explosion.transform.rotation);
    }
}
