﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PaintBrush : MonoBehaviour, IPickup<Transform>
{ 
	CanvasList canvas;

    public bool use;

    byte eventCode = 3;
    List<object> content = new List<object>();
    RaiseEventOptions eventTarget = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
    SendOptions sendOptions = new SendOptions { Reliability = true };

    public void UsePickup(Transform transform)
    {
        PhotonNetwork.RaiseEvent(eventCode, content, eventTarget, sendOptions);
    }

    // Update is called once per frame
    void Update()
    {
        if (use)
        {
            UsePickup(this.transform);
            use = false;
        }
    }
}
