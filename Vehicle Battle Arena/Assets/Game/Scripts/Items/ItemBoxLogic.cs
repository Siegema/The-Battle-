﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ItemBoxLogic : MonoBehaviour
{
    public float spawnTimer = 5.0f;
    public float rotationSpeed = 12.0f;
    public bool isHit = false;

    private List<GameObject> items;
    private Text itemIconText;
    private GameObject itemManager;
    private float spawnTimerSaved;
    private GameObject selectedItem;
    private BoxCollider boxCollider;
    private MeshRenderer meshRenderer;

    public void RotateBox()
    {
        transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed);
        transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
    }

    public void OnTriggerEnter(Collider other)
    {
        // TODO: Finish this for Networking
        PhotonView otherpv = other.GetComponentInParent<PhotonView>();
        //if (otherpv != null && otherpv.IsMine)
        //We Only care if it's a player and maybe his inventtory empty
        if(other.gameObject.GetComponentInParent<PlayerLogic>() && otherpv.IsMine)
        {
            AssignRandomItem(other.gameObject);
            Disappear();
            isHit = true;
        }
    }

    public void AssignRandomItem(GameObject player)
    {
        if (player.GetComponentInParent<PlayerLogic>().currentItem == null)
        {
            selectedItem = items[Random.Range(0, items.Count)];
            Debug.Log(selectedItem.name + " was selected.");
            player.GetComponentInParent<PlayerLogic>().currentItem = selectedItem;
            //itemIconText.text = selectedItem.name;
            
            
        }
    }

    public void Disappear()
    {
        boxCollider.enabled = false;
        meshRenderer.enabled = false;
    }

    public void Reappear()
    {
        boxCollider.enabled = true;
        meshRenderer.enabled = true;
    }

    void Start()
    {
        boxCollider = this.GetComponent<BoxCollider>();
        meshRenderer = this.GetComponent<MeshRenderer>();
        spawnTimerSaved = spawnTimer;
        itemManager = GameObject.Find("ItemManager");
        items = itemManager.GetComponent<ItemManager>().itemList;
        itemIconText = itemManager.GetComponent<ItemManager>().ItemIconText;
    }

    void Update()
    {
        if (isHit)
        {
            spawnTimer -= Time.deltaTime;

            if (spawnTimer <= 0.0f)
            {
                Reappear();
                spawnTimer = spawnTimerSaved;
                isHit = false;
            }
        }
        else
        {
            RotateBox();
        }
    }
}
