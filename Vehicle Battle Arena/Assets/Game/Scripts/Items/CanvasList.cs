﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasList : MonoBehaviour
{ 
    public GameObject paintPanel;

    // Use this for initialization
    void Start()
    { 
        Transform panel = transform.Find("Obstruction");
        if (panel)
        {
            paintPanel = panel.gameObject;
            paintPanel.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
