﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerCameras : MonoBehaviour
{ 
    public GameObject[] cameras;
    public GameObject cameraParent;

    public GameObject mainCamera;

    public int currentCamera;

    public GameObject rampCamera;

    PhotonView pv;

    // Use this for initialization
    private void Start()
    {
        currentCamera = 0;

        pv = GetComponent<PhotonView>();
        if (pv != null)
        {
            if (!pv.IsMine)
            {
                cameraParent.SetActive(false);
            }
            else
            {
                cameras[0].SetActive(true);
            }
        }

        CameraMngr.instance.playerCams.Add(cameraParent);
    }

    private void OnDestroy()
    {
       
        CameraMngr.instance.playerCams.Remove(cameraParent);
    }

    // Update is called once per frame
    private void NextCamera()
    {
        int nextCamera = (currentCamera + 1 >= cameras.Length) ? 0 : currentCamera + 1;

        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].SetActive(i == nextCamera);
        }

        currentCamera = nextCamera;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            NextCamera();
        }

        OnRamp();
    }

    private void OnRamp()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, Vector3.down);

        if(Physics.Raycast(ray, out hit, 10.0f))
        {
            float angle = Vector3.Angle(Vector3.up, hit.normal);
            Debug.DrawRay(transform.position, Vector3.down * hit.distance, Color.red);
            //Debug.Log(angle);

            if(angle > 15)
            {
                rampCamera.gameObject.SetActive(true);
                Debug.Log("Should Activate other camera");
            }
            else
            { 
                rampCamera.gameObject.SetActive(false);
            }
        }
    }
}
