﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

//TODO: Remove this script

public class CarColor : MonoBehaviour
{
    // These colors must be in the same order as the CarColourSelect script
    public Color[] colors;

    private PhotonView PV;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        if(!PV)
        {
            Debug.LogWarning("View not set");
        }
        else
        {
            if (PV.IsMine)
            {
                PV.RPC("RPC_SetColor", RpcTarget.All, Room.instance.colorIndex);
            }
        }
    }

    [PunRPC]
    public void RPC_SetColor(int index)
    {
        //GetComponent<Renderer>().material.mainTexture = textures[index];
        Renderer[] parts = GetComponentsInChildren<Renderer>();

        foreach (Renderer part in parts)
        {
            foreach (Material mat in part.materials)
            {
                if (mat != null && mat.name.Contains("Steel"))
                {
                    mat.SetColor("_Color", colors[index]);
                }
            }
        }
    }
}
