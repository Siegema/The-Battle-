﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GetItemStateBehaviour : StateMachineBehaviour
{
    private Transform target;
    private NavMeshAgent agent;
    private PlayerLogic player;

    private EnemyAILogic logic;

    override public void OnStateEnter(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log(fsm.gameObject.transform.parent.name + "'s State: GetItem.");
        player = fsm.gameObject.GetComponentInParent<PlayerLogic>();
        agent = fsm.gameObject.GetComponentInParent<NavMeshAgent>();
        logic = fsm.gameObject.GetComponent<EnemyAILogic>();
        target = logic.GetNearestItemBox(logic.itemBoxes);

        if (target == null)
        {
            //if all itemBoxes are hidden, default to the first
            target = logic.itemBoxes[0];
        }
    }

    override public void OnStateUpdate(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (target.gameObject.activeInHierarchy == false)
        {
            target = logic.GetNearestItemBox(logic.itemBoxes);
        }

        if (player.currentItem != null)
        {
            fsm.SetBool("hasItem", true);
        }
        else
        {
            agent.destination = target.position;
        }
    }
}