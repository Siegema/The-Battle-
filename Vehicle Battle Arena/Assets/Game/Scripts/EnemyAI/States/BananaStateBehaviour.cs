﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BananaStateBehaviour : StateMachineBehaviour
{
    public float distanceFromItembox = 5.0f;

    public GameObject target;
    public NavMeshAgent agent;
    public PlayerLogic player;
    public EnemyAILogic logic;

    override public void OnStateEnter(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log(fsm.gameObject.transform.parent.name + "'s State: Banana.");
        player = fsm.gameObject.GetComponentInParent<PlayerLogic>();
        agent = fsm.gameObject.GetComponentInParent<NavMeshAgent>();
        logic = fsm.gameObject.GetComponent<EnemyAILogic>();
        target = logic.GetNearestItemBox(logic.itemBoxes).gameObject;
    }

    override public void OnStateUpdate(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (player.currentItem != null)
        {
            if (logic.GetDistanceFromTarget(target) <= distanceFromItembox)
            {
                player.UseItem();
                fsm.SetBool("hasItem", false);
                fsm.SetBool("hasBanana", false);
            }
            else
            {
                agent.destination = target.transform.position;
            }
        }
    }
}