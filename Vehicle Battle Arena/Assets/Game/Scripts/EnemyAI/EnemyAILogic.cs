﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAILogic : MonoBehaviour
{
    public List<Transform> itemBoxes;
    public List<GameObject> enemyList;
    public GameObject lastItemboxHit;
    private GameObject parent;
    private PlayerLogic playerLogic;

    private void Awake()
    {
        parent = this.gameObject.transform.parent.gameObject;
        playerLogic = this.gameObject.GetComponentInParent<PlayerLogic>();
        GetAllItemBoxPositions();
    }

    public void GetAllItemBoxPositions()
    {
        foreach (ItemBoxLogic target in FindObjectsOfType(typeof(ItemBoxLogic)))
        {
            itemBoxes.Add(target.transform);
        }
    }

    public void GetAllPlayers()
    {
        foreach (PlayerLogic target in FindObjectsOfType(typeof(PlayerLogic)))
        {
            if (target.gameObject != parent)
            {
                enemyList.Add(target.gameObject);
            }
        }
    }

    public Transform GetNearestItemBox(List<Transform> boxes)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in boxes)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                if (potentialTarget.GetComponent<ItemBoxLogic>().isHit == false)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestTarget = potentialTarget;
                }
            }
        }
        return bestTarget;
    }

    public GameObject GetNearestTarget(List<GameObject> targets)
    {
        GameObject bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (GameObject potentialTarget in targets)
        {
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }
        return bestTarget.gameObject;
    }

    public GameObject GetLowestHPTarget()
    {
        GameObject bestTarget = null;
        int lowestHPTarget = enemyList[0].gameObject.GetComponent<PlayerLogic>().currentHealth; //get the first one by default for comparison
        foreach (GameObject potentialTarget in enemyList)
        {
            int potentialTargetHealth = potentialTarget.gameObject.GetComponent<PlayerLogic>().currentHealth;
            if (potentialTargetHealth <= lowestHPTarget)
            {
                bestTarget = potentialTarget;
            }
        }
        return bestTarget;
    }

    public GameObject GetHighestHPTarget()
    {
        GameObject bestTarget = null;
        int highestHPTarget = enemyList[0].gameObject.GetComponent<PlayerLogic>().currentHealth; //get the first one by default for comparison
        foreach (GameObject potentialTarget in enemyList)
        {
            int potentialTargetHealth = potentialTarget.gameObject.GetComponent<PlayerLogic>().currentHealth;
            if (potentialTargetHealth >= highestHPTarget)
            {
                bestTarget = potentialTarget;
            }
        }
        return bestTarget;
    }

    public GameObject DetermineChaseTarget()
    {
        //int targetChoice = Random.Range(0, 2);
        int targetChoice = 0;
        GameObject target;

        switch (targetChoice)
        {
            case 0:
                Debug.Log("Target: Lowest HP.");
                //target = GetHighestHPTarget();
                return GetHighestHPTarget(); ;
            case 1:
                Debug.Log("Target: Highest HP.");
                return GetLowestHPTarget();
            case 2:
                Debug.Log("Target: Nearest.");
                return GetNearestTarget(enemyList);
            default:
                Debug.Log("Target: No enemies left.");
                return enemyList[0];
        }
    }

    public float GetDistanceFromTarget(GameObject target)
    {
        float distanceFromTarget = Vector3.Distance(target.transform.position, parent.transform.position);
        return distanceFromTarget;
    }

    public bool IsTargetInFront(GameObject target)
    {
        var heading = target.transform.position - parent.transform.position;
        float dot = Vector3.Dot(heading, parent.transform.forward);

        if (dot > 0.5f)
        {
            return true;
        }

        return false;
    }
}