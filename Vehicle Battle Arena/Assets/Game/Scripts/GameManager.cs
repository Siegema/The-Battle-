﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public string levelToLoad;
    public static GameManager Instance;
    public GameObject mainMenuPanel;
    public Camera menuCamera;

    #region Private Fields

    private Scene GlobalScene;  // always persistent
    private Scene CurrentScene; // the scene we are currently playing
    private Scene PrevScene;    // the scene we were playing just before now

    #endregion

    #region MonoBehaviour Callbacks
    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable()
    {
        if (Instance == null)
            Instance = this;

        // keep track of the GlobalScene
        GlobalScene = SceneManager.GetActiveScene();
        CurrentScene = GlobalScene;

        // we need to respond top scene loaded and sceneunloaded
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    #endregion

    #region Public Methods

    // Need this for pause menu
    public Scene getCurrentScene()
    {
        return CurrentScene;
    }

    public void LoadGameScene()
    {
        mainMenuPanel.SetActive(false);
        SceneManager.LoadSceneAsync(levelToLoad, LoadSceneMode.Additive);
    }

    public void OnSceneLoaded(Scene newScene, LoadSceneMode mode)
    {
        Debug.Log("We just loaded " + newScene.name);

        PrevScene = CurrentScene;
        CurrentScene = newScene;

        SceneManager.SetActiveScene(CurrentScene);

        // do we need to do any cleanup here
        // if we just loaded another scene that is not 'global'
        // we should unload previous scene
        if (PrevScene.name != "GLOBAL")
        {
            if (PrevScene.buildIndex >= 0)
            {
                SceneManager.UnloadSceneAsync(PrevScene);
                Debug.Log("We just Unloaded " + PrevScene.name);
            }
        }
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    #endregion
}
