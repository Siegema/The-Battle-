﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonSelected : EventTrigger
{
    private Color selectedColor = new Color32(143, 0, 254, 255);
    private Color deselectedColor = new Color(0, 0, 0);

    private Text buttonText;

    private void Start()
    {
        buttonText = GetComponentInChildren<Text>();
    }

    public override void OnSelect(BaseEventData eventData)
    {
        buttonText.color = selectedColor;
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        buttonText.color = deselectedColor;
    }
}
