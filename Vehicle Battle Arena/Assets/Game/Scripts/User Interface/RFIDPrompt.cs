﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;

public class RFIDPrompt : MonoBehaviour
{
    public CarColourSelect cc;

    public GameObject promptButton;
    public GameObject cancelButton;

    SerialPort sp;

    public List<GameObject> testObjects;

    private bool _continue;

    void Start()
    {
        promptButton.SetActive(false);
        cancelButton.SetActive(false);

        string comPort = FindArduino();

        if (comPort != null || comPort == "")
        {
            sp = new SerialPort(comPort, 9600);
            promptButton.SetActive(true);
        }
    }

    void Update()
    {

    }

    public void SpawnObject()
    {
        cancelButton.SetActive(true);
        _continue = true;
        sp.Open();
        sp.ReadTimeout = 500;

        while (_continue)
        {
            try
            {
                int index = sp.ReadByte();

                switch (index)
                {
                    case 1:
                        cc.SetColour(cc.carColours.Length - 1);
                        _continue = false;
                        break;

                    case 2:
                        cc.SetColour(cc.carColours.Length - 2);
                        _continue = false;
                        break;

                    default:
                        break;
                }
            }
            catch (System.TimeoutException)
            {
                Debug.LogWarning("Arduino Timed Out");
            }
        }

        sp.Close();
    }

    public void CancelRFID()
    {
        _continue = false;
    }

    string FindArduino()
    {
        string[] ports = SerialPort.GetPortNames();

        if (ports.Length == 0)
        {
            return null;
        }

        return ports[0];
    }
}
